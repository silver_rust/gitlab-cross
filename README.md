# Background
I like gitlab, and I like their CI process.  
I had originally tried to do my own script for cross compiling but that could only work with GNU.

I found out [cross](https://github.com/rust-embedded/cross) exists which uses docker to compile to different targets.  
Thankfully [another person](https://github.com/rust-embedded/cross/issues/273#issuecomment-528300479) made a template 
to build a docker image and how to integrate it into gitlab ci.

So I used that as a base, changing the base image to be the official rust image, installing cross and slightly modifying 
the script to use the env variables that the rust team setup.


# Usage
To use it yourself checkout [example.gitlab-ci.yml](example.gitlab-ci.yml)